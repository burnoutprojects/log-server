require('dotenv').config()
const mongoose = require('mongoose')

const { Schema } = mongoose

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })

const LogSchema = new Schema({
  level: {
    type: String,
    required: true,
    enum: [ 'log', 'warn', 'info', 'error' ]
  },
  enviroment: {
    type: String,
    required: true,
    enum: [ 'development', 'testing', 'production' ]
  },
  message: {
    type: String,
    required: true
  },
  created_at: {
    type: Date,
    default: new Date().toISOString()
  }
})

module.exports = mongoose.model('Log', LogSchema)
