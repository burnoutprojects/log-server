const router = require('express').Router()

const Log = require('./models/log')

// Return specified log entry
router.get('/:id', async (req, res) => {
  const { id } = req.params

  try {
    const { _id, level, enviroment, message, created_at } = await Log.findById(id).exec()
    res.json({
      id: _id,
      level,
      enviroment,
      message,
      created_at
    })
  } catch (error) {
    res.status(400).json(error)
  }
})

// Return all logs
router.get('/', async (_req, res) => {
  try {
    const logs = await Log.find({})
    const response = logs.map(log => {
      const { _id, level, enviroment, message, created_at } = log
      return {
        id: _id,
        level,
        enviroment,
        message,
        created_at
      }
    })
    res.json(response)
  } catch (error) {
    res.status(400).json(error)
  }
})

// Create new log entry
router.post('/', async (req, res) => {
  const { level, enviroment, message } = req.body
  try {
    await Log.create({ level, enviroment, message })
    res.status(204).end()
  } catch (error) {
    res.status(400).json(error)
  }
})

// Delete a log entry
router.delete('/:id', async (req, res) => {
  const { id } = req.params

  try {
    await Log.findOneAndDelete(id)
    res.status(204).end()
  } catch (error) {
    res.status(400).json(error)
  }
})

// Delete all logs entries
router.delete('/', async (_req, res) => {
  try {
    await Log.deleteMany({})
    res.status(204).end()
  } catch (error) {
    res.status(500).json(error)
  }

})

module.exports = router
