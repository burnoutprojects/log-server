require('dotenv').config()
const express = require('express');

const routes = require('./router')

const PORT = process.env.PORT || 8000
const app = express()

app.use(express.json())

app.use('/logs', routes)

app.listen(PORT, () => {
    console.log('Log server listening on ' + PORT);
});
