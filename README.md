This is just a simple log server powered by Nodejs and MongoDB

# Client Side Usage

```bash
# Sending log to server
POST /logs

# Parameters (JSON)
{
  level: ['log', 'warn', 'info', 'error']
  enviroment: ['development', 'testing', 'production']
  message: String
}

# Retrieve log with ID from server
GET /logs/:id

# Retrieve all logs from server
GET /logs

# Delete log with ID from server
DELETE /logs/:id

# Clear all logs from server
DELETE /logs
```

# Server Side Usage

Installation:

```bash
git clone https://gitlab.com/kamenos96/log-server
npm install
```

Env Variables:

```bash
export PORT=8000
export MONGODB_URI="mongodb://127.0.0.1:27017/logs"

or

Make an `.env` file
PORT=8000
MONGODB_URI="mongodb://127.0.0.1:27017/logs"
```

Start:

```
node app.js
# Or use a process manager
pm2 start --name="log-server" node app.js
```

# Source

https://gitlab.com/kamenos96/log-server
